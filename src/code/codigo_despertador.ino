//Bibliotecas usadas
#include <Adafruit_NeoPixel.h>
#include <LiquidCrystal.h>
#include <Keypad.h>

//Configuração do painel de lcd, teclado  e fita led

#define LED 6
#define LED_COUNT 12
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
Adafruit_NeoPixel strip = Adafruit_NeoPixel(LED_COUNT, LED, NEO_GRB + NEO_KHZ800);

const byte ROWS = 4;
const byte COLS = 4;
char keys[ROWS][COLS] = {{'1', '2', '3', 'A'},  {'4', '5', '6', 'B'},  {'7', '8', '9', 'C'},{'*', '0', '#', 'D'}};
byte rowPins[ROWS] = {10, 9, 8, 7};
byte colPins[COLS] = {A1, A2, A3, A4};
int LCDcol = 0;

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

//Variavies

void tela(String mostrai);
String setarhora(String hora2,String mostra);
void printaContador(int h, int m);


void setup() {
  Serial.begin(9600);
  lcd.begin(16, 2);
  strip.begin();
  strip.clear();
  strip.show();

}


void loop() { 
  
//limpa a tela e os leds para reinicio do loop
  lcd.clear();
  strip.clear();
  strip.show();
  
  String mostra="===Hora atual===",mostra2="===Alarme hora==",hora, hora2;
 
  tela(mostra);
  hora=setarhora(hora,mostra);
  delay(200);
  tela(mostra2);
  hora2=setarhora(hora2,mostra2);

//Converção da hora informada (String) pra horas e minutos(inteiros)

  int hAtual =  hora.toInt() / 100;
  int hAlarme = hora2.toInt() / 100;
  int mAtual =  hora.toInt() % 100;
  int mAlarme = hora2.toInt() % 100;

  lcd.clear();
  
//Loop que incrementa a hora até ela chegar na hora desejada
  
  for (;;)
  {
    lcd.setCursor(0, 0);
    lcd.print("=====Alarme=====");
    if (mAtual == 60)
    {
      hAtual ++;
      if (hAtual == 24)
      {
        hAtual = 0;
        mAtual = 0;
        lcd.clear();
      }
      else
        mAtual = 0;
    }
    if (mAtual == mAlarme && hAtual == hAlarme) {
      break;
    }
    printaContador(hAtual, mAtual);
    delay(250);
    mAtual ++;
  }
  lcd.setCursor(0, 0);
  lcd.print("====Hora====");
  lcd.setCursor(0, 1);
  lcd.print("===De Acordar===");
  int brilho, vetor[12] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
 
//Loop para acender o led gradualmente
  
  for (brilho = 0; brilho <= 64; brilho++)
  {
    char tecla = keypad.getKey();
    if (tecla == '*') {
      brilho = 65;
    }
    else {
      strip.setBrightness( brilho );
      strip.setPixelColor(vetor[1], 100, 100, 10);
      strip.setPixelColor(vetor[brilho], 100, 100, 10);
      strip.show();
    }
   delay(500);
  }
}

//Fução printar a horas até o despertar

void printaContador(int h, int m){
  lcd.setCursor(5, 1);
  lcd.print(h);
  lcd.setCursor(7, 1);
  lcd.print(":");
  lcd.setCursor(8, 1);
  lcd.print(m);
}

//Função que seta as horas

String setarhora(String hora2,String mostra){
  tela(mostra);
  int ii=5;
  do {
    char tecla = keypad.getKey();
    if (tecla) {
      if (tecla == 'A' || tecla == 'B' || tecla == 'C' || tecla == '#') {
        ii = 5;
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("=Tecla Ivalida=");
        lcd.setCursor(0, 0);
        delay(1000);
        lcd.clear();
        lcd.print(mostra);
        lcd.setCursor(8, 1);
        lcd.print(":");
        lcd.setCursor(ii, 1);
      }
      if (tecla == 'D') {
        ii = 5;
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print(mostra);
        lcd.setCursor(7, 1);
        lcd.print(":");
        lcd.setCursor(ii, 1);
        hora2 = "";

      }
      else if (ii == 7) {
        lcd.setCursor(8, 1);
        lcd.print(tecla);
        hora2 += tecla;
        ii += 2;
      }
      else {
        lcd.print(tecla);
        hora2 += tecla;
        ii++;
      }
      lcd.setCursor(ii, 1);
    }
  } while (ii <  10);
   return hora2;
  }

//Função para escolher inforção exibida na tela

void tela(String mostrai){
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(mostrai);
  lcd.setCursor(7, 1);
  lcd.print(":");
  lcd.setCursor(5, 1);
}
