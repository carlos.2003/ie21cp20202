# Introdução
Este projeto é um protótipo de um despertador que emite luz, e por meio dessa desperta o usuário de uma forma mais sutil.
## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

|Nome| gitlab user|
|---|---|
|Carlos Ribas|carlos.2003|
|Claudio Fischer|Claudio_Fischer|

# Documentação

A documentação do projeto pode ser acessada pelo link:

> https://carlos.2003.gitlab.io/ie21cp20202/.

# Links Úteis

* [Kit de Desenvolvimento Arduino](https://www.arduino.cc/)
* [Sintax Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* [Hugo Website](https://gohugo.io/)

